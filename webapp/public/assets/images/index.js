const router = require('express-promise-router')();
const controller = require('./controller');
const Middleware = require('../middleware');

router.get('/initial',
  controller.initial,
  Middleware.sendResponse
);

module.exports = router;
