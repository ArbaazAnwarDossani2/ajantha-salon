import React from 'react';
import {
  Link
} from 'react-router-dom';

const Navbar = (props) => (
  <header className="site-header">
    <div className="container">
      <a title="Ajantha Beauty Salon | India" id="branding">
        <img style={{width:40}} src="./assets/images/logo.png" alt="Ajantha Beauty Salon | India" className="logo" />
        <div className="logo-copy">
          <h1 className="site-title">Ajantha Salon & Academy</h1>
          <small className="site-description">Established 1975</small>
        </div>
      </a>
      <div className="main-navigation">
        <button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>
        <ul>
          <li>
              <Link to="/">Home
                <span class="line1"></span>
                <span class="line2"></span>
                <span class="line3"></span>
                <span class="line4"></span>
              </Link>
          </li>
          <li className="menu-item"><Link to="/services">Services</Link></li>
          <li className="menu-item"><Link to="/gallery">Gallery</Link></li>
          <li className="menu-item"><Link to="/contact">Contact</Link></li>
        </ul>
      </div>

      <div className="mobile-navigation"></div>
    </div>
  </header>
)

export default Navbar;
