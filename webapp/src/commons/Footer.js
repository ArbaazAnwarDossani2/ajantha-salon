import React from 'react';

const Footer = (props) => (
  <footer className="site-footer" style={{background:'black'}}>
    <div className="container">
      <div className="row">
        <div className="col-md-3">
          <div className="widget contact-widget">
            <h3 className="widget-title">Contact</h3>
            <div className="contact-info">
              <address>
                <i className="fa fa-map-marker icon"></i>
                <p><strong style={{color:"#ec5598"}}>Ajantha Salon</strong> Shop No.12,Konark Tower,Ghantali Road, Behind 3 Petrol pump Naupada,Thane (W) Thane 400062</p>
              </address>
              <a title="Ajantha Beauty Salon | India" className="mail"><i className="fa fa-envelope icon"></i>ajanthabeautysalon@gmail.com</a>
              <a title="Ajantha Beauty Salon | India" className="phone"><i className="fa fa-phone icon"></i>02264528858 / 9167943313 / 9167944271</a>
            </div>
          </div>
        </div>
        <div className="col-md-3">
          <div className="widget">
            <h3 className="widget-title">Social Media</h3>
            <p>To get the Latest update about Ajantha Salon, Follow and Subscribe us on our Social Media Links Below</p>
            <div className="social-links">
              <a title="Ajantha Beauty Salon | India" target="_new" href="https://www.facebook.com/Ajantha-salon-Academy-Family-Salon-1494576034127729/"><i className="fa fa-facebook"></i></a>
            </div>
          </div>
        </div>
        {/* <div className="col-md-5">
          <div className="widget">
            <h3 className="widget-title">Newsletter</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident et praesentium </p>
            <form action="#" className="subscribe-form">
              <input type="email" placeholder="Enter our email..." />
              <input type="submit" value="Sign up" />
            </form>
          </div>
        </div> */}
      </div>

      <div className="colophon">
        <p>Copyright 2017 Ajantha Salon. Developed by A&A. All rights reserved.</p>
      </div>
    </div>
  </footer>
)

export default Footer;
