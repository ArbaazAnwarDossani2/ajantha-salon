import React, { Component } from 'react';
// import './App.css';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import Footer from '../commons/Footer';
import Home from './Home';
import Services from './Services';
import Gallery from './Gallery';
import Contact from './Contact';

const history = createBrowserHistory();

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div style={{background:'black'}}>
          <Route exact path="/" component={Home} />
          <Route path="/services" component={Services} />
          <Route path="/gallery" component={Gallery} />
          <Route path="/contact" component={Contact} />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
