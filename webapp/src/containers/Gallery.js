import React from 'react';
import {
  Link
} from 'react-router-dom';

class Gallery extends React.Component{
  render() {
    return (
      <div>
        <header className="site-header">
          <div className="container">
            <a id="branding">
              <img style={{width:40}} src="./assets/images/logo.png" alt="Company name" className="logo" />
              <div className="logo-copy">
                <h1 className="site-title">Ajantha Salon & Academy</h1>
                <small className="site-description">Established 1975</small>
              </div>
            </a>
            <div className="main-navigation">
              {/*<button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>*/}
                <div className=".menu-main-menu-container">
                  <ul id="menu-main-menu" className="abc nav navbar-nav pull-right ">
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Home</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/services">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Services</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/gallery">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Gallery</p>
                            <span className="line1 line1Active"></span>
                            <span className="line2 line2Active"></span>
                            <span className="line3 line3Active"></span>
                            <span className="line4 line4Active"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/contact">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Contact</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>

                  </ul>
                </div>
            </div>
            <div className="mobile-navigation"></div>
          </div>
        </header>
        <main className="main-content">
  				<div className="page">
  					<div className="container">
  						<div className="text-center">
  							<div className="filter-links filterable-nav">
  								<select className="mobile-filter">
  									<option value="*">Show all</option>
  									<option value=".hair">hair</option>
  									<option value=".manicure">manicure</option>
  									<option value=".pedicure">pedicure</option>
  									<option value=".face">face</option>
  									<option value=".makeup">makeup</option>
  									<option value=".misc">miscellaneous</option>
  								</select>
  								<a title="Ajantha Beauty Salon | India" className="current wow fadeInRight" data-filter="*">Show all</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay=".2s" data-filter=".hair">hair</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay=".4s" data-filter=".manicure">manicure</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay=".6s" data-filter=".pedicure">pedicure</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay=".8s" data-filter=".face">face</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay="1s" data-filter=".makeup">makeup</a>
  								<a title="Ajantha Beauty Salon | India" className="wow fadeInRight" data-wow-delay="1s" data-filter=".misc">miscellaneous</a>
  							</div>
  						</div>

  						<div className="filterable-items">
                <div className="gallery-item filterable-item misc">
                  <a title="Ajantha Beauty Salon | India" href="assets/images/diwali/1.jpeg">
                    <figure className="featured-image">
                      <img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/1.jpeg" />
                    </figure>
                  </a>
                </div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/2.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/2.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/3.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/3.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/4.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/4.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/5.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/5.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/6.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/6.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/7.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/7.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/8.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/8.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/9.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/9.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/10.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/10.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/11.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/11.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/12.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/12.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/13.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/13.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/14.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/14.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/15.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/15.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/16.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/16.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/17.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/17.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/18.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/18.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/19.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/19.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/20.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/20.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/21.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/21.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/22.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/22.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/23.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/23.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/24.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/24.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/25.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/25.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/26.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/26.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/27.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/27.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/diwali/28.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/diwali/28.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-1.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-1.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-2.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-2.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-3.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-3.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-4.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-4.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-5.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-5.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-6.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-6.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-7.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-7.jpeg" />
  									</figure>
  								</a>
  							</div>
  							<div className="gallery-item filterable-item hair">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-8.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-8.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-9.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-9.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-10.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-10.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-11.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-11.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-12.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-12.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-13.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-13.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-14.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-14.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-15.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-15.jpeg" />
  									</figure>
  								</a>
  							</div><div className="gallery-item filterable-item manicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-16.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-16.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-17.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-17.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-18.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-18.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-19.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-19.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-20.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-20.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-21.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-21.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-22.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-22.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-23.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-23.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item pedicure">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-24.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-24.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-25.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-25.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-26.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-26.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-27.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-27.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-28.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-28.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-29.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-29.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-30.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-30.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-31.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-31.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item face">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-32.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-32.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-33.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-33.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-34.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-34.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-35.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-35.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-36.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-36.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-37.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-37.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-38.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-38.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-39.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-39.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item makeup">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-40.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-40.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-41.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-41.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-42.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-42.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-43.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-43.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-44.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-44.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-45.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-45.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-46.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-46.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-47.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-47.jpeg" />
  									</figure>
  								</a>
  							</div>
                <div className="gallery-item filterable-item misc">
  								<a title="Ajantha Beauty Salon | India" href="assets/images/gallery/gallery-48.jpeg">
  									<figure className="featured-image">
  										<img alt="Ajantha Beauty Salon | India" src="assets/images/gallery/gallery-48.jpeg" />
  									</figure>
  								</a>
  							</div>
  						</div>
  					</div>
  				</div>
  			</main>
      </div>
    )
  }
}

export default Gallery;
