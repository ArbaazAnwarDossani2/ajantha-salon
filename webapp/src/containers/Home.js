import React from 'react';
import { Carousel } from 'antd';
import {
  Link
} from 'react-router-dom';
// import Carousel from '../components/Carousel';
import OurServices from '../components/OurServices';
import OurTeam from '../components/OurTeam';

class Home extends React.Component{
  render() {
    return (
      <div>
        <header className="site-header">
          <div className="container">
            <a id="branding">
              <img style={{width:40}} src="./assets/images/logo.png" alt="Company name" className="logo" />
              <div className="logo-copy">
                <h1 className="site-title">Ajantha Salon & Academy</h1>
                <small className="site-description">Established 1975</small>
              </div>
            </a>
            <div className="main-navigation">
              {/*<button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>*/}
                <div className=".menu-main-menu-container">
                  <ul id="menu-main-menu" className="abc nav navbar-nav pull-right ">
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Home</p>
                          <span className="line1 line1Active"></span>
                          <span className="line2 line2Active"></span>
                          <span className="line3 line3Active"></span>
                          <span className="line4 line4Active"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/services">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Services</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/gallery">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Gallery</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/contact">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Contact</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>

                  </ul>
                </div>
            </div>

          </div>
        </header>

        <Carousel autoplay>
          <li style={{ backgroundImage: 'url("./assets/images/carousel21.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <h3 className="slider-subtitle" style={{ paddingTop: 75, color:'black' }}>Dussehra Offer</h3>
              <h2 className="slider-title">Flat 30% off on all services.</h2>
            </div>
          </li>
          <li style={{ backgroundImage: 'url("./assets/images/carousel15.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <h3 className="slider-subtitle" style={{ paddingTop: 75, color:'black' }}>Wanna Join Us?</h3>
              <h2 className="slider-title">Become A Member</h2>
              <p className="slider-description" style={{color:'black'}}>And enjoy 30% discount on All our Service for this month</p>
            </div>
          </li>
          <li style={{ backgroundImage: 'url("./assets/images/offerasset0.png")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <h3 className="slider-subtitle" style={{ paddingTop: 75, color:'black' }}>Ajantha Salon Presents</h3>
              <h2 className="slider-title">Happy Hours</h2>
              <p className="slider-description" style={{color:'black'}}>Every Monday to Friday 1pm to 5pm</p>
            </div>
          </li>
          <li style={{ backgroundImage: 'url("./assets/images/slider-1.jpg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <p className="slider-subtitle" style={{ paddingTop: 75 }}>Let Us Offer You</p>
              <h2 className="slider-title">The Best Beauty Services</h2>
              <p className="slider-description">We specialize at high-quality beauty services that range from skin care to hair coloring and styling. Our stylists will be glad to provide you with an amazing look!</p>
            </div>
          </li>
          <li style={{ backgroundImage: 'url("./assets/images/carousel4.jpeg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <h3 className="slider-subtitle" style={{ paddingTop: 75 }}>Take care of Yourself</h3>
              <h2 className="slider-title">With our Comprehensive Help</h2>
              <p className="slider-description">Everyday we help hundreds of Clients look gorgeous. With our services and extensive beauty care, you can always look and feel great!</p>
            </div>
          </li>
          <li style={{ backgroundImage: 'url("./assets/images/carousel6.jpeg")', backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 700 }}>
            <div className="container">
              <h3 className="slider-subtitle" style={{ paddingTop: 75 }}>Enjoy the Ultimate</h3>
              <h2 className="slider-title">Beauty Salon Experience</h2>
              <p className="slider-description">You can experience a wide range of services aimed at gaining your new captivating lookand style you always wanted to have!</p>
            </div>
          </li>
        </Carousel>
        <OurServices />
        <OurTeam />
      </div>
    )
  }
}

export default Home;
