import React from 'react';
import {
  Link
} from 'react-router-dom';
import ServiceJson from '../json/service.json'
import { Collapse, Radio } from 'antd';
const Panel = Collapse.Panel;


class Services extends React.Component{

  constructor() {
    super();
    this.state = {
      services: [],
      selectedType: 'Male'
    }
  }

  componentDidMount() {
    this.setState({
      services: ServiceJson.services
    })
  }

  handleGenderChange(e) {
    this.setState({
      selectedType: e.target.value
    })
  }

  render() {
    return (
      <div>
        <header className="site-header">
          <div className="container">
            <a id="branding">
              <img style={{width:40}} src="./assets/images/logo.png" alt="Company name" className="logo" />
              <div className="logo-copy">
                <h1 className="site-title">Ajantha Salon & Academy</h1>
                <small className="site-description">Established 1975</small>
              </div>
            </a>
            <div className="main-navigation">
              {/*<button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>*/}
                <div className=".menu-main-menu-container">
                  <ul id="menu-main-menu" className="abc nav navbar-nav pull-right ">
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Home</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/services">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Services</p>
                            <span className="line1 line1Active"></span>
                            <span className="line2 line2Active"></span>
                            <span className="line3 line3Active"></span>
                            <span className="line4 line4Active"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/gallery">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Gallery</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/contact">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Contact</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>

                  </ul>
                </div>
            </div>

            <div className="mobile-navigation"></div>
          </div>
        </header>
        <div className="container">
          <div className="page">
            <div className="row">
              <div className="col-md-6">
                <Radio.Group value={this.state.selectedType} onChange={(e) => this.handleGenderChange(e)} style={{ paddingBottom: 20, display: 'flex', justifyContent: 'flex-end' }}>
                  <Radio.Button value="Male">Male</Radio.Button>
                  <Radio.Button value="Female">Female</Radio.Button>
                </Radio.Group>
                <Collapse>
                  {this.state.services.length !== 0 && this.state.services.map((service, i) => {
                    return (
                      <Panel header={service.category} key={i}>
                        <div style={{ borderBottom: '1px solid #ccc', marginBottom: 10, display: 'flex' }}>
                          <p style={{ width: '60%', margin: 0}}>Name</p>
                          <p style={{ width: '20%', margin: 0, display: 'flex', justifyContent: 'center' }}>Non-Member</p>
                          <p style={{ width: '20%', margin: 0, display: 'flex', justifyContent: 'center' }}>Member</p>
                        </div>
                        {
                          service.subCategory.length !== 0 && service.subCategory.map((subCategory, j) => {
                            return (
                              <div key={j}>
                                {this.state.selectedType === subCategory.type && <div style={{ borderBottom: '1px solid #ccc', marginBottom: 10, display: 'flex' }}>
                                  <p style={{ width: '60%', margin: 0}}>{subCategory.name}</p>
                                  <p style={{ width: '20%', margin: 0, display: 'flex', justifyContent: 'center' }}>{subCategory.price.NM}</p>
                                  <p style={{ width: '20%', margin: 0, display: 'flex', justifyContent: 'center' }}>{subCategory.price.M}</p>
                                </div>}
                              </div>
                            )
                          })
                        }
                      </Panel>
                    )
                  })}
                </Collapse>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Services;
