import React from 'react';
import {
  Link
} from 'react-router-dom';

class Contact extends React.Component{

  componentDidMount(){
    var script = document.createElement('script');
    script.src = "https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyCFniGfrhVfFToQucfYS78rQ3zK_VY0fQ8&loc:19.192593+72.970665&callback=initMap";
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  _loadingFinished(event){
    if(event){
      var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
      document.getElementById("name").value = "";
      document.getElementById("mobile").value = "";
      document.getElementById("service").value = "";
      document.getElementById("message").value = "";
    }
  }

  render() {
    return (
      <div>
        <header className="site-header">
          <div className="container">
            <a id="branding">
              <img style={{width:40}} src="./assets/images/logo.png" alt="Company name" className="logo" />
              <div className="logo-copy">
                <h1 className="site-title">Ajantha Salon & Academy</h1>
                <small className="site-description">Established 1975</small>
              </div>
            </a>
            <div className="main-navigation">
              {/*<button type="button" className="menu-toggle"><i className="fa fa-bars"></i></button>*/}
                <div className=".menu-main-menu-container">
                  <ul id="menu-main-menu" className="abc nav navbar-nav pull-right ">
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Home</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/services">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Services</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/gallery">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Gallery</p>
                          <span className="line1"></span>
                          <span className="line2"></span>
                          <span className="line3"></span>
                          <span className="line4"></span>
                        </Link>
                      </a>
                    </li>
                    <li id="menu-item-25" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                      <a style={{padding:6, margin:0}}>
                        <Link to="/contact">
                          <p style={{ color: '#ec5598', fontSize:17, padding:"0px 5px", margin:0}}>Contact</p>
                            <span className="line1 line1Active"></span>
                            <span className="line2 line2Active"></span>
                            <span className="line3 line3Active"></span>
                            <span className="line4 line4Active"></span>
                        </Link>
                      </a>
                    </li>

                  </ul>
                </div>
            </div>
            <div className="mobile-navigation"></div>
          </div>
        </header>
        <main className="main-content">
  				<div className="page">
  					<div className="container">
  						<p style={{color:'#2c2c2c'}}>Founded in 1975, Ajantha Salon remains one of the oldest, matured and the most trusted Salon when it comes to experience in handling your beauty and Wellness. We are a full service hair salon located in midtown Thane. We believe that looking your best leads to feeling your best and we are passionate about helping you unlock your beauty potential. Our team of hair specialists will cut, color, shape and style your hair into the best it can be. We know a great haircut isn’t a cure-all, but sometimes it’s all it takes to take YOU to the next level. </p>
  						<div className="row">
  							<div className="col-md-4">
                  <iframe name="hidden_iframe" title="hidden_iframe" id="hidden_iframe" style={{display:'none'}} onLoad={(event) => this._loadingFinished(event)}></iframe>
                  <form className="contact-form" action="https://docs.google.com/forms/d/e/1FAIpQLSeGBnoSk4XUylNiE1SI1DuD1Is65tzcCWjuimd_-oW_Z-vmKA/formResponse" target="hidden_iframe" method="POST" id="mG61Hd">
  									<input type="text" id="name" placeholder="Name..." aria-label="Name" aria-describedby="i.desc.1565625030 i.err.1565625030" name="entry.1961729905" dir="auto" data-initial-dir="auto" data-initial-value=""/>
  									<input type="text" id="mobile" placeholder="Mobile..." tabIndex="0" aria-label="Number" aria-describedby="i.desc.1753633296 i.err.1753633296" name="entry.365101791" dir="auto" data-initial-dir="auto" data-initial-value=""/>
  									<input type="text" id="service" placeholder="Service..." tabIndex="0" aria-label="Service" aria-describedby="i.desc.1733679192 i.err.1733679192" name="entry.291028463" dir="auto" data-initial-dir="auto" data-initial-value=""/>
                    {/* <input type="text" id="date" placeholder="Date(DD-MM-YYYY)" /> */}
  									<textarea name="" id="message" placeholder="Message..."data-rows="1" tabIndex="0" aria-label="Message" name="entry.1158177501" dir="auto" data-initial-dir="auto" data-initial-value="" ></textarea>
  									<div className="text-right">
  										<button className="button large" type="submit" aria-disabled="false">Send Message</button>
  									</div>
  								</form>
  							</div>
  							<div className="col-md-6 col-md-offset-1">
  								<div className="map-container">
  									<div className="map" id="map"></div>
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
          <div id="snackbar">Thank You. We will get back to you Shortly</div>
  			</main>
      </div>
    )
  }
}

export default Contact;
