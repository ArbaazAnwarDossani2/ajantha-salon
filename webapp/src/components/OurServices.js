import React from 'react';

const OurServices = (props) => (
  <div className="fullwidth-block features-section">
    <div className="container">
      <h2 className="section-title">our Services</h2>
      <div className="row">
        <div className="col-md-3a">
          <div className="feature">
            <img alt="Ajantha Beauty Salon | India" src="./assets/images/icon-1.png" className="feature-image" />
            <h3 className="feature-title">Nail Art</h3>
            <p>A girl should have two things: Beautiful Nails and Fabulous Toes! <br/> - Tammy Taylor</p>
          </div>
        </div>
        <div className="col-md-3a">
          <div className="feature">
            <img alt="Ajantha Beauty Salon | India" src="./assets/images/icon-2.png" className="feature-image" />
            <h3 className="feature-title">Massage</h3>
            <p>I always think I look better after a yoga class. It's the same as a massage. We look so amazing after a massage because we're relaxed. <br/> - Andie MacDowell</p>
          </div>
        </div>
        <div className="col-md-3a">
          <div className="feature">
            <img alt="Ajantha Beauty Salon | India" src="./assets/images/icon-3.png" className="feature-image" />
            <h3 className="feature-title">Hair Services</h3>
            <p>The beauty of a woman is not in the clothes she wears, the figure that she carries or the way she combs her hair. <br/> - Audrey Hepburn</p>
          </div>
        </div>
        <div className="col-md-3a">
          <div className="feature">
            <img alt="Ajantha Beauty Salon | India" src="./assets/images/icon-4.png" className="feature-image" />
            <h3 className="feature-title">Facial Care</h3>
            <p>Normally for photo shoots I get a full wax, some tanning, a facial. <br/> - Elizabeth Banks</p>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default OurServices;
