import React from 'react';
import { Carousel } from 'antd';

const Carousel = (props) => (
  // <div className="hero hero-slider">
  //   <ul className="slides">
  //     <li style={{ backgroundImage: 'url("./assets/images/slider-1.jpg")' }}>
  //       <div className="container">
  //         <h3 className="slider-subtitle">Let Us Offer You</h3>
  //         <h2 className="slider-title">The Best Beauty Services</h2>
  //         <p style={{color:"white", fontSize: 23}}>We specialize at high-quality beauty services that range from skin care to hair coloring and styling. Our stylists will be glad to provide you with an amazing look!</p>
  //         {/*<a title="Ajantha Beauty Salon | India" className="button large">Read more</a>*/}
  //       </div>
  //     </li>
  //     <li style={{ backgroundImage: 'url("./assets/images/slider-2.jpg")' }}>
  //       <div className="container">
  //         <h3 className="slider-subtitle">Take care of Yourself</h3>
  //         <h2 className="slider-title">With our Comprehensive Help</h2>
  //         <p style={{color:"white", fontSize: 23}}>Everyday we help hundreds of Clients look gorgeous. With our services and extensive beauty care, you can always look and feel great!</p>
  //         {/*<a title="Ajantha Beauty Salon | India" className="button large">Read more</a>*/}
  //       </div>
  //     </li>
  //     <li style={{ backgroundImage: 'url("./assets/images/slider-3.jpg")' }}>
  //       <div className="container">
  //         <h3 className="slider-subtitle">Enjoy the Ultimate</h3>
  //         <h2 className="slider-title">Beauty Salon Experience</h2>
  //         <p style={{color:"white", fontSize: 23}}>You can experience a wide range of services aimed at gaining your new captivating lookand style you always wanted to have!</p>
  //         {/*<a title="Ajantha Beauty Salon | India" className="button large">Read more</a>*/}
  //       </div>
  //     </li>
  //   </ul>
  // </div>
)

export default Carousel;
