import React, { Component } from 'react';
import { Carousel } from 'antd';

class OurTeam extends Component {

  render() {
    return(
      <div className="fullwidth-block team-section">
        <div className="container">
          <h2 className="section-title">Our team</h2>
            <Carousel autoplay>
              <li style={{ backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 450 }}>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-1.gif" /></figure>
                    <h3 className="team-name">Neha</h3>
                    <p>Awesome Entrepreneur! Equally great at management as she is with her stylist hands. Her Communication with customers is one to watchout for!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-2.gif" /></figure>
                    <h3 className="team-name">Naseem</h3>
                    <p>Dedicated with Ajantha for over a couple of decades. She has been the backbone of this team for a long while!!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-3.gif" /></figure>
                    <h3 className="team-name">Sagar</h3>
                    <p>His Magical hands will transform your looks into one which you never imagined possible! He is young, energetic and always willing to learn.</p>
                  </div>
                </div>
              </li>
              <li style={{ backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 450 }}>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-4.gif" /></figure>
                    <h3 className="team-name">Shahin</h3>
                    <p>Hard to distinguish whether she is great with Facial or Hair treatments! As of our concern, she is a gem of the two worlds!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-5.gif" /></figure>
                    <h3 className="team-name">Mayuri</h3>
                    <p>Hardworking, Dedicated and stops at nothing but perfection to leave a wonderful smile at our client's face!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-6.gif" /></figure>
                    <h3 className="team-name">Rahul</h3>
                    <p>Naam toh Suna hoga! Here, however, the name comes with wonderful Facial and Hair Services!</p>
                  </div>
                </div>
              </li>
              <li style={{ backgroundRepeat: 'no-repeat', backgroundSize: 'cover', minHeight: 450 }}>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-7.gif"  /></figure>
                    <h3 className="team-name">Afreen</h3>
                    <p>One heck of a Manager. She works really hard to convert our Walk-in clients to our Permanent Member!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-8.gif"  /></figure>
                    <h3 className="team-name">Swati</h3>
                    <p>Her Experience is really valuable and adds real value to the team. Her calmness in chaotic seasons is amazing!</p>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="team">
                    <figure className="team-image"><img alt="Ajantha Beauty Salon | India" src="./assets/images/staff/staff-10.gif"  /></figure>
                    <h3 className="team-name">Tahir</h3>
                    <p>Tahir is new to the Team, but his hospitality with our Clients makes him a vital Asset!</p>
                  </div>
                </div>
              </li>
            </Carousel>
        </div>
      </div>
    )
  }
}

export default OurTeam;
